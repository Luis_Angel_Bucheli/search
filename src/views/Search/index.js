import { useState } from "react";
import SearchBox from "./components/SearchBox";
import data from "../../data/users.json"
import "./styles.css";
import SearchResults from "./components/SearchResults";

export default function Search() {
  const [isAtTop, setIsAtTop] = useState(false);
  const [userData, setUserData] = useState(data);
  const [result, setResult] = useState([]);
  console.log(result);
  const handleSearchClick = (searchText) => {
    setIsAtTop(true);
    if (userData?.length) {
      setIsAtTop(true)
      const searchTextMinus = searchText.toLowerCase()
      const filteredData = userData.filter((value) => (
          value.name.toLocaleLowerCase().includes(searchTextMinus) ||
          value.phone.toLocaleLowerCase().includes(searchTextMinus) ||
          value.email.toLocaleLowerCase().includes(searchTextMinus) ||
          value.username.toLocaleLowerCase().includes(searchTextMinus)
        )
      )
      setResult(filteredData);
    }
    
  }
  const handleCloseClick = () => {
    setIsAtTop(false)
    setResult([])
  }

  return(
    <div className={`search ${isAtTop ? "search--top" : "search--center"}`}>
      <SearchBox onSearch={handleSearchClick} onClose={handleCloseClick} isSearching={ isAtTop } />
      <SearchResults results={ result } isSearching={ isAtTop } />
    </div>
  );
}